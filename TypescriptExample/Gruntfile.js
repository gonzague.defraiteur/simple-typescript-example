module.exports = function (grunt) {

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		src: ["./src/**/*.ts"],
		dest: "./build/TacticalRPG.js",
		options: {
			module: 'amd',
			target: 'es5',
			basePath: './src',
			sourceMap: true,
			declaration: true
		},
		generateTsConfig: true,

		ts: {
            default : {
				tsconfig: './tsconfig.json',
				outfile: "./build/outfile.js"
            }
		},
		watch: {
			scripts: {
			  files: ['src/**/*.ts'],
			  tasks: ['tsc'],
			  outfile: './build/outfile.js',
			  options: {
				spawn: false,
			  },
			},
		  }
	});
	grunt.loadNpmTasks('grunt-exec');
	grunt.loadNpmTasks('grunt-contrib-less');
	var path = require('path');
    var fs = require('fs');

    function getTscVersion(tscPath) {
        const pkg = JSON.parse(fs.readFileSync(path.resolve(tscPath, '..', 'package.json')).toString());
        return '' + pkg.version;
    }

    function ensureDirectoryExistence(filePath) {
        var dirname = path.dirname(filePath);
        if (fs.existsSync(dirname)) {
            return true;
        }
        fs.mkdirSync(dirname);
    }
	grunt.registerTask('watch', 'Compile but watch', function(prop)
	{
		grunt.task.run('tsc');
	});
	grunt.registerTask('--watch', 'Compile but watch', function(prop)
	{
		grunt.task.run('tsc');
		
	});
    grunt.registerTask('tsc', 'Compile typescript', function (prop) {

        //grunt.log.writeln('Running tsc:' + this.target);

        let options = {};// this.data.options || {};
        let tscPath = path.join('node_modules', 'typescript', 'bin');
        let tscBin = path.join(tscPath, 'tsc');

        grunt.log.writeln("tsc task options: " + JSON.stringify(options));

        let args = [];

        if (options.files != null && options.files.length > 0) {
            args = args.concat(options.files);
        }
		args.push("--watch");
        if (options.project != null) {
            args.push('--project', options.project);
        } else {
            if (options.files == null) {
                args.push('--project', 'tsconfig.json');
            }
        }

        if (options.watch != null && options.watch == true) {
            args.push('--watch');
        }
        //let watch = grunt.config.get('watch');
        // grunt.log.writeln('WATCH 22?? #' + watch);
        // if (watch) {
        //     args.push('--watch');
        // }

        let randid = Math.floor(Math.random() * (100000 - 1)) + 1;
        let tempfilename = 'build/tscommand' + randid + '.tmp';
        ensureDirectoryExistence(tempfilename);
        fs.writeFileSync(tempfilename, args.join(' '));

        grunt.log.writeln("tsc task args: " + args.join(' '));

        var done = this.async();
        var child = grunt.util.spawn({
            cmd: process.execPath,
            args: [tscBin, '@' + tempfilename],
            opts: {
                cwd: "."
            }
        }, function (err, result, code) {
            if (err) {
                grunt.log.error(">> Failed to compile: " + err);
                fs.unlinkSync(tempfilename);
                done(false);
            } else {
                grunt.log.writeln(">> Compilation terminated");
                fs.unlinkSync(tempfilename);
                done(true);
            }
        });
        child.stdout.pipe(process.stdout);
        child.stderr.pipe(process.stderr);
    });
    // Ex: setConfig:target:staging
	grunt.registerTask('default', ['tsc']);
}