var MAIN;
(function (MAIN) {
    // cette variable peut etre lue depuis d'autres fichiers!
    MAIN.someVar = "yoyoyoyoyoyoyoy";
})(MAIN || (MAIN = {}));
/// <reference path="./OtherFileExample.ts"/>
// au dessus ca veut dire: référence le fichier OtherFileExample.ts, pour que je puisse me servir ici de ce qu'il y a dedans.
// toujours mettre tes classes (ou ton code) dans un module pour l'organisation, parceque sinon tu vas confondre avec d'autres librairies.
var MAIN;
/// <reference path="./OtherFileExample.ts"/>
// au dessus ca veut dire: référence le fichier OtherFileExample.ts, pour que je puisse me servir ici de ce qu'il y a dedans.
// toujours mettre tes classes (ou ton code) dans un module pour l'organisation, parceque sinon tu vas confondre avec d'autres librairies.
(function (MAIN) {
    var Main = /** @class */ (function () {
        function Main() {
            document.getElementById("hello_world").textContent = "HELLO WORLD!!!" + " " + MAIN.someVar;
        }
        return Main;
    }());
    MAIN.Main = Main;
    MAIN.main = new Main();
})(MAIN || (MAIN = {}));
// on aurait pu juste faire ça ici:
// document.getElementById("hello_world").textContent = "HELLO WORLD!!!" + " " + MAIN.someVar; 
// mais c'est plus rigolo de le mettre dans une classe.
//# sourceMappingURL=build.js.map