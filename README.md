# Simple Typescript Example

# Pré-Requis:

git,

https://git-scm.com/download/win

nodejs:

https://nodejs.org/en/

(prendre "recommanded for all users")



# Pour L'Utiliser,

commencer par faire un clone de ce projet:

dans une ligne de commande, faire:

git clone https://gitlab.com/gonzague.defraiteur/simple-typescript-example.git

ça va créer un dossier "simple-typescript-example" avec le contenu de ce projet.



aller dans le dossier "simple-typescript-example", 
aller dans le dossier "TypescriptExample"


faire npm install

(ca va creer un dossier "node_modules", qui contient les modules nodejs spécifiés dans le fichier "package.json")

faire grunt --watch

(ca va faire tourner en boucle le compilateur typescript, si tu modifie un fichier ".ts", il régénérera des fichiers ".js")


lancer un serveur local:

(si vous n'avez pas de quoi le faire, a l'aide d'une autre ligne de commande, faites "npm install http-server -g"
	ensuite, dans le dossier racine du projet ("simple-typescript-example"), faites "http-server -c-1 -p 8088" 
)

voila, vous pouvez maintenant aller sur localhost:8088/TypescriptExample/ et voir votre premiere page html, utilisant typescript.


# Modifier le code,

pour modifier le code, ouvrez le dossier "simple-typescript-example" depuis vscode, et modifiez "index.html", ou bien les fichiers typescript contenus dans le dossier "src"

